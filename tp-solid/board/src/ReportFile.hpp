#pragma once

#include "Itemable.hpp"

class ReportFile{
    private:
        std::ofstream _ofs;

    public:
        ReportFile(const std::string & filename) : _ofs(filename) {}
        void reportFile(const Itemable& itemable){
            for (const std::string & item : itemable.getItems())
                _ofs << item << std::endl;
                _ofs << std::endl;
        }
}
