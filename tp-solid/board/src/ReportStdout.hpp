#pragma once

#include "Itemable.hpp"

class ReportStdout{
    public:
        void reportStdout(const Itemable& itemable) {
            for (const std::string & item : itemable.getItems())
                std::cout << item << std::endl;
                std::cout << std::endl;
            }
};
