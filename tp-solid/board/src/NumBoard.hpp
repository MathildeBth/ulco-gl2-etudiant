#include "Board.hpp"

class NumBoard : public Board{
    private:
        int nextNum;

    public:
        NumBoard():nextNum(1){};
        void add(const std::string & t) override {
            Board::add(std::to_string(nextNum) + ". " + t);
           nextNum++;
        }
        virtual std::string getTitle() const override {
            return "NumBoard";
        }
};
