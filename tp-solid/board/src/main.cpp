
#include "Board.hpp"
#include "NumBoard.hpp"
#include "ReportFile.hpp"
#include "ReportStdout.hpp"

void testBoard(Board & b) {
    std::cout << b.getTitle() << std::endl;
    b.add("item 1");
    b.add("item 2");
    ReportStdout report;
    report.reportStdout(b);
    ReportFile report2("tmp2.txt");
    report2.reportFile(b);
}

int main() {

    Board b1;
    NumBoard b2;
    
    testBoard(b2);

    return 0;
}

