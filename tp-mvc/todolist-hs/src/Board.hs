module Board where

import Data.List (partition)
import Task

data Board = Board
    { _boardId :: Int
    , _boardTodo :: [Task]
    , _boardDone :: [Task]
    }

newBoard :: Board
newBoard = Board 1 [] []


addTodo :: String -> Board -> (Int, Board)
addTodo str (Board i ts ds) = (i, b)
    where b = Board (i+1) (ts++[t]) ds
          t = Task i str


toDone :: Int -> Board -> Board
toDone i0 (Board i ts ds) = Board i ts' (ds++ds')
    where (ds', ts') = partition ((==i0) . _taskId) ts
