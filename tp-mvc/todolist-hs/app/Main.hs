import Board
import View

-- import Text.Read (readMaybe)

loop :: Board -> IO ()
loop b = do
    putStrLn ""
    printBoard b
    line <- getLine
    case words action of
        ("add": xs) -> loop (snd $ addTodo (unwords xs) b)
        -- done ...
        ("quit": _) -> return ()
        _ -> loop b

