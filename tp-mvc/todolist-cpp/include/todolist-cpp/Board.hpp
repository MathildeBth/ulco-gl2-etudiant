#pragma once

#include "Task.hpp"

#include <string>
#include <vector>

class Board {
    private:
        //déclaration attributs
        int boardId;
        std::vector<Task> boardTodo;
        std::vector<Task> boardDone;

    public:
        //constructeur
        Board(int boardId) : boardId(boardId) {}
        //ajouter
        void addTodo(std::string str) {
            boardTodo.push_back(Task((boardId + 1),str));
        }
        //terminé
        void toDone(int taskId) {
            //faire si getBoardId = taskId  
            boardTodo.push_back(Task(0,""));
        }
        //guetters
        int getBoardId() { return boardId; }
        std::vector<Task> getBoardTodo() { return boardTodo; }
        std::vector<Task> getBoardDone() { return boardDone; }
}
