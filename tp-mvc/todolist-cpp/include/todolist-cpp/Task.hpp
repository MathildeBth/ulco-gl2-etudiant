#pragma once

#include <stdlib.h>
#include <string>

class Task{
    private:
    //déclaration attributs
        int taskId;
        std::string taskName;

    public:
    //constructeur
    Task(int taskId, std::string taskName) : taskId(taskId), taskName(taskName) {}
    //guetters
    int getTaskId() { return taskId; }
    std::string getNameTask() { return taskName; }
}