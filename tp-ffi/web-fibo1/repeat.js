function repeatN(n, f) {
    var li = [];
    for (var i = 0; i < n; ++i) {
        li[i] = f(i);
    }
    return li;
}
