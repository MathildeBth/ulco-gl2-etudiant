{-# LANGUAGE ForeignFunctionInterface #-}

import Foreign.C.Types

foreign import ccall "mul2"
    c_mul2 :: CInt -> CInt

foreign import ccall "add3"
    c_add3 :: CInt -> CInt -> CInt

hs_mul2 :: Int -> Int
hs_mul2 x = fromIntegral(c_mul2 (fromIntegral x))

add3 :: Int -> Int -> Int
add3 x y = fromIntegral(c_add3 (fromIntegral x) (fromIntegral y))

main :: IO ()
main = do
    putStrLn "this is myapp"
    print $ hs_mul2 21
    print $ add3 21 21

