
function mul2(n) {
    return n*2;
}

function make_handler(myspan, myinput){
    return function(){
        myspan.innerHTML = mul2(myinput.value);
    }
}


