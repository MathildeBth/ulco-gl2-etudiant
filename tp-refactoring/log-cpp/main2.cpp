#include <fstream>
#include <functional>
#include <iostream>

using logFunc_t = std::function<void(const std::string &)>;

int add3(int n) {
    return n+3;
}

int mul2(int n) {
    return n*2;
}

int mycompute(logFunc_t f,int v0) {
    f("add3 " + std::to_string(v0));
    const int v1 = add3(v0);
    f( "mul2 " + std::to_string(v1));
    const int v2 = add3(v1);
    return v2;
}

int main() {
    std::cout << "this is log-cpp" << std::endl;
    auto f1 = [](const std::string & m) {std::cout << m << std::endl;};
    int r1 = mycompute(f1, 18);
    std::cout << r1 << std::endl;

    auto f2 = [](const std::string &) {};
    int r2 = mycompute(f2, 18);
    std::cout << r2 << std::endl;

    std::ofstream ofs("log.txt");
    auto f3 = [&ofs](const std::string & m) {
        ofs << m << std::endl;
    };
    int r3 = mycompute(f3, 18);
    std::cout << r3 << std::endl;

    return 0;
}

